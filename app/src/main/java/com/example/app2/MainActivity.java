package com.example.app2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.example.app2.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        EditText edittext = findViewById(R.id.edittext1);
//        edittext.setText("France");
//        Button button = findViewById(R.id.button);
//        button.setText("Cliquez sur le button");
//        ImageView image = findViewById(R.id.image);
//        image.setImageResource(R.drawable.flag);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        init();
    }



    private void init() {
        binding.image.getLayoutParams().height = (int) getResources().getDimension(R.dimen.picture_height);
        binding.image.getLayoutParams().width = (int) getResources().getDimension(R.dimen.picture_width);
        binding.image.setBackground(getResources().getDrawable(R.drawable.flag_of_france));
        binding.hello.setText(getResources().getText(R.string.picture_name));
        Intent intent = getIntent();
        String text = intent.getStringExtra("text2");
        binding.edittext1.setText(text);
        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("LE BUTTON", "On clique");
                String text = binding.edittext1.getText().toString();
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("text1", text);
                startActivity(intent);
            }
        });
    }
}