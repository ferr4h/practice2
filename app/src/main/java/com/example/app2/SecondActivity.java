package com.example.app2;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.example.app2.databinding.ActivitySecondBinding;

public class SecondActivity extends AppCompatActivity {

    EditText textView;
    ActivitySecondBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySecondBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        textView = binding.edittext2;

        Intent intent = getIntent();
        String text = intent.getStringExtra("text1");
        textView.setText(text);

    }

    ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult());
    
    public void onActivityResult(ActivityResult result){
        if (result.getResultCode() == AppCompatActivity.RESULT_OK){
            Intent data = result.getData();
            String returnedData = data.getStringExtra("key");
            Log.i(TAG, "Возвращение данных:" + returnedData);
        }
        else{
            Log.i(TAG, "Нет данных");
        }
    }
    
    
    public void click(View view) {
        Log.i("LE BUTTON", "On clique");
        String text = binding.edittext2.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("text2", text);
        setResult(RESULT_OK, intent);
        mStartForResult.launch(intent);
        finish();
    }
}
